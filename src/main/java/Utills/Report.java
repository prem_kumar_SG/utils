package Utills;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;


public class Report {
	public static int failStep;
	public static String excelLog, htmlLog;
	public static String HTMLFileLocation;
	public static String testFailed;
	
	public static File screenShotFolder;
	public static File LogHTMLFolder ;
	
	/*Report(File Screenshot,File HTMLLog)
	{
		 screenShotFolder = Screenshot;
		 LogHTMLFolder = HTMLLog;
		
	}*/
	
	public static String takeScreenshot(WebDriver driver) {
		String screenShotName = new SimpleDateFormat("_yy-MMM-dd-HH-mm-ss").format(Calendar.getInstance().getTime());
				
//		File screenShotFolder = null;//Webdriver.getScreenShotFolder();
		if (driver instanceof TakesScreenshot) {
			File tempFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try{
				FileUtils.copyFile(tempFile, new File(screenShotFolder+"/" + screenShotName + ".jpg"));
			}catch (IOException e) {
			}
		}
		return screenShotFolder + "/" + screenShotName + ".jpg";
	}
	
	public static void Fail(WebDriver driver, String stepDescription){
		testFailed = "Failed";
		excelLog=excelLog + (++failStep) + " . " + stepDescription+"\n";
		Report.appendFail(stepDescription + "<br>Landed Page Title is : " + driver.getTitle() ,takeScreenshot(driver));
	}
	
	public static void writeToLog(String ProjectName){
		htmlLog=htmlLog+"</ul></font>";
		htmlLog = htmlLog +"</body></html>";
		//File LogHTMLFolder = Webdriver.getClientLogHTMLFolder();
		
		try{
			HTMLFileLocation = LogHTMLFolder.getName();
	        String HTMLFile = LogHTMLFolder + "//"+ProjectName.replaceAll("/", ".")+".html";
	        File file = new File(HTMLFile);
	        FileUtils.writeStringToFile(file, htmlLog, true);
	     }catch(IOException e){
	    	System.out.println("File creation error : " + e.getMessage());
		}finally{
			if(Report.testFailed.contains("Fail")){
				Reporter.log("Failed for client : " + ProjectName);
				Assert.fail("Some of test steps are failed - please look into html log");
			}
		}
	}
	
	public static void ReportHeader(String ReportName){
		htmlLog = "<html><body><font color=gold><h1>"+ReportName + "</font></h1>";//<br>";
	}
	
	public static void TestCaseHeader(String TestCaseName){
		htmlLog = htmlLog + "<font color=blue><H3>"+TestCaseName+"</H3></font><font color=maroon>";//<ul>";
	}
	public static void TestCaseSubTitle(String SubTestCaseName){
		htmlLog = htmlLog + "<font color=maroon><H3>"+SubTestCaseName+"</H3></font>";
	}
	public static void Pass(String stepDescription){
		failStep++;
		htmlLog = htmlLog + "<br><font color=Green>" + failStep + ". " + stepDescription+ "<font color=red>" + "</font>";
	}
	
	public static void appendFail(String stepDescription, String screenShot){
		htmlLog = htmlLog + "<br><font color=red>" + failStep + ". " +stepDescription + "- <a href=" + screenShot + "> Click Here For Screen Shot </a></font>";
	}
}