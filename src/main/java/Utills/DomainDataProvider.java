package Utills;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import jxl.Cell;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;



public class DomainDataProvider {
	private static List<DomainList> domainList;
	private static String domainsheet;
	private static String URL;
	
    @DataProvider(name="DomainListData")
    public static Iterator<Object[]> fileDomainDataProvider(ITestContext context) {
    	
    	//Get the input file path from the ITestContext
    	String domainsFilePath = context.getCurrentXmlTest().getParameter("domainsFilePath");
    	String environmentSheetName = context.getCurrentXmlTest().getParameter("environmentSheetName");
    	
    	String productionSheetName = context.getCurrentXmlTest().getParameter("domainsSheetName");
    	String stagingSheetName = context.getCurrentXmlTest().getParameter("stagingSheetName");
    	String qaSheetName = context.getCurrentXmlTest().getParameter("qaSheetName");
    	
    	ExcelUtils readFromExcel=new ExcelUtils(domainsFilePath);
    	
    	domainsheet  = setEnvironmentDetails(readFromExcel,environmentSheetName);
    	
    	if(domainsheet.contains("staging")){
    		readFromExcel.setSheet(stagingSheetName);
    		URL = readFromExcel.getCellValue("E2");
    	}else if(domainsheet.contains("qa")){
    		readFromExcel.setSheet(qaSheetName);
    		URL = readFromExcel.getCellValue("E2");
    		
    	}else{
    		readFromExcel.setSheet(productionSheetName);	
    		URL = readFromExcel.getCellValue("E2");
    	}

        //Get a list of String file content (line items) from the test file.
       // List<DomainList> domainList = getFileContentList(readFromExcel);
		domainList = getFileContentList(readFromExcel);
 
        //We will be returning an iterator of Object arrays so create that first.
        List<Object[]> domainListReturned = new ArrayList<Object[]>();
 
        //Populate our List of Object arrays with the file content.
        for (DomainList domainListItem : domainList)
        {
        	domainListReturned.add(new Object[] { domainListItem } );
        }
        //return the iterator - testng will initialize the test class and calls the 
        //test method with each of the content of this iterator.
        System.out.println("read file");
        return domainListReturned.iterator();
 
    }

    public static String setEnvironmentDetails(ExcelUtils readFromExcel,String sheetName){
    	String selection=null;
    	try{
    		readFromExcel.setSheet(sheetName);
    		selection =  readFromExcel.getCellValue("B4");
    	}catch(Exception e){
    		readFromExcel.close();	
    	}
    	
    	if(selection.contains("Staging"))
    		return "staging";
    	else if(selection.contains("QA"))
    		return "qa";
    	else
    	    return "production";
     }
    
	public static List<DomainList> getFileContentList(ExcelUtils readFromExcel){
		List<DomainList> domainList=new ArrayList<DomainList>();
		DomainList dl;
		
		try{
			Cell[] domain=readFromExcel.getCell(0);
			Cell[] status=readFromExcel.getCell(1);
			Cell[] clientMailID = readFromExcel.getCell(2);
			Cell[] browsername = readFromExcel.getCell(3);
			Cell[] keyword = readFromExcel.getCell(4);
			System.out.println("Domains Length : " + domain.length);
			for(int i=1;i<domain.length;i++){
				dl=new DomainList();
		
				dl.setDomainName(domain[i].getContents().trim());
				dl.setStatus(status[i].getContents().trim());
				dl.setClientID(clientMailID[i].getContents().trim());
				dl.setBrowserName(browsername[i].getContents().trim());
				dl.setKeyword(keyword[i].getContents().trim());
				if(status[i].getContents().trim().equals("true"))
					domainList.add(dl);
			}
			
		}catch(Exception e){
			readFromExcel.close();
		}
		return domainList;
	}
	
	public static int getDomainCount(){
		return domainList.size();
	}
	
	public static String getProductionURL(){
		return URL;
	}
}
