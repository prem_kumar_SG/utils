package Utills;

/*

S.No  Class name		Created by	Created Date	Purpose							Modifed By	Modified Date
**************************************************************************************************
1. 	   ExcelUtils		Prem kumar	24/May/2017		To handle Excel	for framework
**/

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;


public class ExcelUtils {
	
	public static Workbook workbook;
	public static Sheet sheet;
	
	
	/**
	 Constructor name : ExcelUtils
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To set the File path and to open the Excel file using JXL for purpose of the framework
	 Modified By :
	 Modified Date :
	 */
	public ExcelUtils(String fileName){
		File file=new File(fileName);                          
		WorkbookSettings workbookSettings=new WorkbookSettings();  
		workbookSettings.setLocale(new Locale("en","EN"));  
        try{
			workbook=Workbook.getWorkbook(file,workbookSettings);
		}catch (BiffException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}   
         
  	}
	
	/**
	 Method name : setSheet
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To set the name of the sheet
	 Modified By :
	 Modified Date :
	 */
	public void setSheet(String sheetName){
		sheet = workbook.getSheet(sheetName);
	}
	
	/**
	 Method name : getCell
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To set the cells of the column
	 Modified By :
	 Modified Date :
	 */
	public Cell[] getCell(int col){
		return sheet.getColumn(col);
	}
	
	/**
	 Method name : getCellValue
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To get the value of the cell instead of Row and column
	 Modified By :
	 Modified Date :
	 */
	public String getCellValue(String cellAddress){
		return sheet.getCell(cellAddress).getContents().trim();
	}
	
	/**
	 Method name : close
	 Created By : Prem Kumar
	 Created Date : 24/May/2017
	 Purpose : To close the excel
	 Modified By :
	 Modified Date :
	 */
	public void close(){
		try{
			workbook.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
